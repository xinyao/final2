from PIL import Image
import sys
import transforms

def parse_color_list(color_list_str):
    colors = []
    for color_str in color_list_str.split(":"):
        r, g, b = map(int, color_str.split(","))
        colors.append((r, g, b))
        return colors

def image_to_dict(img):
    return {
        "width": img.width,
        "height": img.height,
        "pixels": list(img.getdata())
    }

def dict_to_image(img_dict):
    img = Image.new("RGB", (img_dict["width"], img_dict["height"]))
    img.putdata(img_dict["pixels"])
    return img

def main():
    if len(sys.argv) < 3:
        print("Usage: transform_multi.py <image_file> <transformation> [<args>...] ...")
        return
    image_file = sys.argv[1]
    operations = sys.argv[2:]

    img = Image.open(image_file).convert("RGB")
    img_data = image_to_dict(img)
    i = 0
    while i < len(operations):
        transformation = operations[i]
        if transformation == "change_colors":
            old_colors = parse_color_list(operations[i+1])
            new_colors = parse_color_list(operations[i+2])
            img_data = transforms.change_colors(img_data, old_colors, new_colors)
            i += 3

        elif transformation == "rotate":
            direction = operations[i+1]
            img_data = transforms.rotate(img_data, direction)
            i += 2

        elif transformation == "shift":
            x_shift = int(operations[i+1])
            y_shift = int(operations[i+2])
            img_data = transforms.shift(img_data, x_shift, y_shift)
            i += 3

        elif transformation == "crop":
            x = int(operations[i+1])
            y = int(operations[i+2])
            width = int(operations[i+3])
            height = int(operations[i+4])
            img_data = transforms.crop(img_data, x, y, width, height)
            i += 5

        elif transformation == "filter":
            r_factor = float(operations[i+1])
            g_factor = float(operations[i+2])
            b_factor = float(operations[i+3])
            img_data = transforms.filter(img_data, r_factor, g_factor, b_factor)
            i += 4

        elif transformation == "grayscale":
            img_data = transforms.grayscale(img_data)
            i += 1

        elif transformation == "mirror":
            img_data = transforms.mirror(img_data)
            i += 1

        else:
            print("Unknown transformation:", transformation)
            return

    transformed_img = dict_to_image(img_data)
    output_file = image_file.rsplit(".", 1)[0] + "_trans." + image_file.rsplit(".", 1)[1]
    transformed_img.save(output_file)
    print("Transformed image saved as:", output_file)

if __name__ == "__main__": main()




