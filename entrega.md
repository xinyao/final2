#ENTREGA CONVOCATORIA JUNIO

Xinyao Lin

x.lin.2023@alumnos.urjc.es

https://youtu.be/TS-MRq48IF8?si=q8pEOtpaXv6-trPV

Requisitos mínimos cumplidos:
- Programa transforms.py
    - Función Mirror
    - Función Grayscale
    - Función Blur
    - Función Change_colors
    - Función Rotate
    - Función Shift
    - Función Crop
    - Función Filter
    - 
- Programa transform_simple.py
- Programa transform_args.py
- Programa transform_multi.py